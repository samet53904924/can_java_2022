import javax.swing.tree.TreeNode;

public class Main {

    public static void main(String[] args) {
        class Solution {
            int diameter = 0;

            public int diameterOfBinaryTree(TreeNode root) {
                heightOfBinaryTree(root);
                return diameter;
            }

            public int heightOfBinaryTree(TreeNode root) {
                if(root == null) {
                    return 0;
                }


                int leftHeight = heightOfBinaryTree(root);
                int rightHeight = heightOfBinaryTree(root);

                diameter = Math.max(diameter, leftHeight + rightHeight);

                int height = Math.max(leftHeight, rightHeight) + 1;

                return height;

            }
        }
        
    }
}
