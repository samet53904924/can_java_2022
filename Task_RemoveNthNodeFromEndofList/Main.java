public class Main {

    public static void main(String[] args) {

        class Solution {
            public ListNode removeNthFromEnd(ListNode head, int n) {
                if (n == 1) {
                    ListNode traverse, previous = null;
                    traverse = head;
                    while (traverse.next != null) {
                        previous = traverse;
                        traverse = traverse.next;
                    }
                    if (previous == null)
                        return null;
                    previous.next = null;
                    return head;

                } else {
                    int totalNumberOfNodes = 0;
                    ListNode traverse = head;
                    while (traverse != null) {
                        traverse = traverse.next;
                        totalNumberOfNodes++;
                    }

                    traverse = head;
                    int nodesTobeAttached = totalNumberOfNodes - n;
                    if (nodesTobeAttached == 0)
                        return head.next;
                    for (int i = 0; i < nodesTobeAttached; i++)
                        traverse = traverse.next;

                    deleteNode(traverse);
                    return head;

                }

            }

            public void deleteNode(ListNode node) {
                node.val = node.next.val;
                node.next = node.next.next;
            }
        }
    }
}
