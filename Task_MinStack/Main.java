public class Main {

    public static void main(String[] args) {
        class MinStack {

            public class Node {
                public int val;
                public Node min = null;
                public Node prev = null;
                public Node(int val, Node tail) {
                    this.val = val;
                    this.prev = tail;

                    if (tail != null && tail.getMinVal() <= val) {
                        this.min = tail.getMinNode();
                    }
                }

                public final Node getMinNode() {
                    return this.min != null ? this.min : this;
                }


                public final int getMinVal() {
                    return getMinNode().val;
                }

            }

            Node head = null;
            Node tail = null;

            public MinStack() {

                head = new Node(Integer.MAX_VALUE, null);


                tail = head;
            }

            public void push(int val) {

                Node n = new Node(val, tail);

                tail = n;
            }

            public void pop() {
                tail = tail.prev;
            }


            public int top() {
                return tail.val;
            }

            public int getMin() {
                return tail.getMinVal();
            }
        }
    }
}
