import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        class Solution {
            public int[] intersect(int[] nums1, int[] nums2) {



                Map<Integer, Integer> numMap = new HashMap<>();
                List<Integer> numList = new ArrayList<>();
                for(int i = 0; i <nums1.length; i++)
                {
                    numMap.put(i, nums1[i]);
                }
                for(int j = 0; j < nums2.length; j++)
                {
                    if (numMap.containsValue(nums2[j]))
                    {
                        numList.add(nums2[j]);
                    }
                    // remove the value from map
                    numMap.values().remove(nums2[j]);
                }
                int[] return_list = numList.stream().mapToInt(Integer::intValue).toArray();
                return return_list;
            }
        }
    }
}
