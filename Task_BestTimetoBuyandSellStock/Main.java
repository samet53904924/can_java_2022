public class Main {

    public static void main(String[] args) {
        class Solution {
            public int maxProfit(int[] prices) {

                int min = Integer.MAX_VALUE;
                int maxDiff = 0;

                for(int i = 0; i < prices.length; i++){
                    min = Math.min(prices[i] , min);
                    maxDiff = Math.max(prices[i] - min, maxDiff);
                }
                return maxDiff;
            }
        }
    }
}
